# IMDB py
from imdb import IMDb
import pandas as pd
import matplotlib.pyplot as plt

ia = IMDb()

#Initialise the array for all interested tv shows
tv_series_drugs = ['0903747', '1124035', '0117951', '0765429', '0099685', '0180093',  '0910936','0439100' ]

tv_series_whitehouse = ['1856010', '1837576','0200276', '1124035', '1024648', '2334879', '2302755', '1327773']

# Array for storing ids and names of respective series
tv_series_names = {}

# get data for shows
def getData(tv_series):

    # Array for storing the votes count of respective series
    tv_series_votes = {}

    # Array for storing the ratings count of respective series
    tv_series_ratings = {}

    for series in tv_series:

        tv_series_name = ia.get_movie(series)
        tv_series_names[series] = tv_series_name
        print "Processing data for ", tv_series_name

        # print tv_series_name.keys()

        # case for movies
        if tv_series_name['kind'] == 'movie':
            # Intialisation
            title = tv_series_name['title']
            tv_series_votes[title] = {}
            tv_series_ratings[title] = {}

            tv_series_votes[title][tv_series_name['year']] = tv_series_name['votes']
            tv_series_ratings[title][tv_series_name['year']] = tv_series_name['rating']
            continue
        else:
            ia.update(tv_series_name, 'episodes')
            episodes = tv_series_name['episodes']

            ia.update(tv_series_name, 'episodes rating')
            start = 0
            ratings = {}
            tv_series_votes[tv_series_names[series]['title']] = {}
            tv_series_ratings[tv_series_names[series]['title']] = {}
            for season_num in episodes:
                num_episodes_season = len(episodes[season_num])

                #print tv_series_name['episodes rating']
                season_episodes = tv_series_name['episodes rating'][start: start + num_episodes_season]

                votes = 0
                ratings = 0

                tiles_per_season = episodes[season_num]

                for item in season_episodes:
                    votes += item['votes']
                    ratings += item['rating']

                year = tiles_per_season[1]['year']
                tv_series_votes[tv_series_names[series]['title']][int(year)] = votes
                tv_series_ratings[tv_series_names[series]['title']][int(year)] = ratings / num_episodes_season
                start += len(episodes[season_num])

    return tv_series_votes, tv_series_ratings

def drawBarPlot(df):
    ax = df.plot.bar()
    for p in ax.patches:
        if int(p.get_height()) != 0:
            #ax.annotate(int(p.get_height()), (p.get_x() * 1.005, p.get_height() * 1.005))

            ax.annotate(int(p.get_height()),
                        (p.get_x()+p.get_width()/2., p.get_height()),
                        ha='center', va='center', xytext=(0, 10), textcoords='offset points')

    plt.show()


# Drug related shows
print "Processing Shows for Drugs"
tv_series_votes, tv_series_ratings = getData(tv_series_drugs)
df = pd.DataFrame.from_dict(tv_series_votes)
df.fillna(0, inplace=True)
print df
df.to_csv('drugs_imdb.csv')

drawBarPlot(df)




