import pandas as pd
import timeit
import matplotlib.pyplot as plt
import matplotlib.dates as dates

"""Retrieves the total number of visits per year from the whitehouse logs."""
"""Download the log at https://open.whitehouse.gov/api/views/p86s-ychb/rows.csv?accessType=DOWNLOAD (1GB)."""
"""Edit the file variable to match the location of the log file."""


def main():
    file = "data.csv"
    fields = ['APPT_START_DATE']
    dateparse = lambda dates: [pd.datetime.strptime(d, '%m/%d/%y %H:%M') for d in dates]
    print "Reading File"
    df = pd.read_csv(file, usecols=fields, parse_dates=True, date_parser=dateparse, infer_datetime_format=True)
    df['APPT_START_DATE'] = pd.to_datetime(df['APPT_START_DATE'], errors='coerce')
    df['year'] = pd.DatetimeIndex(df['APPT_START_DATE']).year
    #print df
    result = df.groupby('year').size()
    print result
    print type(result)
    result.to_csv('results.csv')
    print "Saving DF to local file"
    df.to_csv("processed_whitehouse1.csv")



#
df = pd.DataFrame.from_csv('data.csv')
#
# print df
#drawBarPlot(df)

df.plot()
plt.ylabel('Years')
plt.xlabel('Number of Visitors')
plt.show()

if __name__ == "__main__":
    start = timeit.default_timer()
    #main()
    stop = timeit.default_timer()
    print stop - start
