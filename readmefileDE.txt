Data Engineering Project : Jupyter Notebook  

____________________________________________

Name : Group #15 Project

____________________________________________

Release data : 2016-07-21

____________________________________________

Contact Information : Universität Stuttgart

____________________________________________

Contributors : 

Mahak Gupta : Internet Movie Database (IMDB) dataset
Monique Marquez : Whitehouse visitors dataset
Natalia Skaczkowska : Drug Enforcement Administration (DEA) dataset

___________________________________________


Requirements :

The library IMDB from Python is (Mahak writes this)
BeautifulSoup
urllib2
pyexcel_ods
_____________________________________________
    
Installation :

The Jupyter Notebook requires the use of Python 2.7 or later. It can be downloaded here : https://www.python.org/download/releases/2.7/

____________________________________________

Usage : 



____________________________________________

Documentation :


(This should be in chronological order) so Mahak, me, then Natalia


WHITEHOUSE VISITORS DATA COLLECTION

whitehouse_visitors.ipynb (Monique) 

The following code retrieves the total number of visits per year from the whitehouse logs and creates a corresponding graph visualizing
the correlation between whitehouse visitor appointments made over a span of six years. 


The following three libraries were imported for proper generation of the 2D visualization for
the whitehouse visitors appointments made over a given year : 

import pandas as pd
import matplotlib.pyplot as plt
import numpy as np


Download the log at https://open.whitehouse.gov/api/views/p86s-ychb/rows.csv?accessType=DOWNLOAD
Edit the variable called file in the notebook to match the location of the log file


The following method faced problems with execution as many of the dates lacked uniform formatting : 

df = pd.read_csv(file, usecols=fields, parse_dates=True, infer_datetime_format=True)

In order to fix this problem a line of code was added to change the extracted date values to data objects. 

This allowed the method to extract dates of varying formats. 

Once the dates are extracted and grouped by year then a plot is made of the data. 

However, a challenge in creating the plotted data was converting the extracted years to a string or in other words something readable as 
they were to be used in the visual plot. Therefore, the years were first converted from float to int, and then converted again from int 
to string. 

The following code is provided here : 

result.index = result.index.astype(int) 
result.index = result.index.astype(str)





____________________________________________

Databases : 


I. Sources


Two of the databases were used from the three suggested by Ralf Diestelkämper. 
This included the Whitehouse visitors database and the IMDB database. The DEA database (third) was found externally. 

___________________________________________

Future Enhancements :

A line of code could be written to retrieve, download and process the data automatically. 

____________________________________________

License : 

Python license: https://www.python.org/download/releases/2.7/license/     


____________________________________________
