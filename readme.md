Data Engineering Project : Jupyter Notebook  

____________________________________________

Name : Group #15 Project

____________________________________________

Release data : 2016-07-21

____________________________________________

Contact Information : Universität Stuttgart

____________________________________________

Contributors : 

* Mahak Gupta : Internet Movie Database (IMDB) dataset

* Monique Marquez : Whitehouse visitors dataset

* Natalia Skaczkowska : Drug Enforcement Administration (DEA) dataset

___________________________________________


# Requirements :

The libraries used in our project are: 

* BeautifulSoup

* urllib2

* pyexcel_ods

* pandas

* imdb 
_____________________________________________
    
# Installation :

The Jupyter Notebook requires the use of Python 2.7 or later. It can be downloaded here : https://www.python.org/download/releases/2.7/

____________________________________________

Usage : 



____________________________________________

# Documentation :



## WHITEHOUSE VISITORS DATA COLLECTION

whitehouse_visitors.ipynb (Monique) 

The following code retrieves the total number of visits per year from the whitehouse logs and creates a corresponding graph visualizing
the correlation between whitehouse visitor appointments made over a span of six years. 

The following three libraries were imported for proper generation of the 2D visualization for the whitehouse visitors appointments made over a given year : 

* import pandas as pd

* import matplotlib.pyplot as plt

* import numpy as np

Download the log at https://open.whitehouse.gov/api/views/p86s-ychb/rows.csv?accessType=DOWNLOAD
Edit the variable called file in the notebook to match the location of the log file

The following method faced problems with execution as many of the dates lacked uniform formatting : 
df = pd.read_csv(file, usecols=fields, parse_dates=True, infer_datetime_format=True)

In order to fix this problem a line of code was added to change the extracted date values to data objects. This allowed the method to extract dates of varying formats. 

Once the dates are extracted and grouped by year then a plot is made of the data. 
However, a challenge in creating the plotted data was converting the extracted years to a string or in other words something readable as 
they were to be used in the visual plot. Therefore, the years were first converted from float to int, and then converted again from int to string. 

The following code is provided here : 

result.index = result.index.astype(int) 
result.index = result.index.astype(str)



## DEA DATA COLLECTION & CLEANING

To obtain data from the DEA, we had to access their website and open the Statistics tab.

The first table, containing information about domestic drug seizures was parsed using BeautifulSoup library. One challenge here was to access the right table (there are two tables on the same page), because of no distinction in their names in the HTML code. The solution was to choose the table based on its width attribute. The content of this table has been saved as a list of lists called "t".

The second dataset has been more problematic and had to be collected manually. The data for "meth lab incidents" was displayed as a set of 10 maps of the US, each map containing the yearly amount of meth lab incidents for each state separately. The numbers were printed on the map and there was no efficient way to extract it, so we collected the numbers manually and created a table in LibreOffice Calc, which is then accessed using pyexcel_ods library. The content of this table has been saved as a list of lists called "data".



## IMDB DATA COLLECTION & CLEANING

Crawl Data from IMDB. We've used imdbpy module to get this data. The returned data is in Json format.

We crawl a bunch of movies and tv series which are already set in a global array for both themes i.e political and drug related.


## DATA INTEGRATION

We're not processing crawled data in our integration phase, we've saved the intermediate data in csv files to make it easier.

We import these csv files directly in pandas dataframe. and then create various plots.

The integration process is same for both themes.


## DATA DISTRIBUTION

The scales for fields we've analysed are different. So it is not really a good idea to deduce correlation in their values merely by looking at the plot. We're using pearson correlation coefficient to deduce these insights.

____________________________________________

Databases : 


I. Sources


Two of the databases were used from the three suggested by Ralf Diestelkämper. 
This included the Whitehouse visitors database and the IMDB database. The DEA database (third) was found externally. 

___________________________________________

Future Enhancements :

A line of code could be written to retrieve, download and process the data automatically. 

____________________________________________

License : 

Python license: https://www.python.org/download/releases/2.7/license/     


____________________________________________